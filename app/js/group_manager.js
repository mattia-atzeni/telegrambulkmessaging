'use strict'

angular.module('myApp.services')

  .service('GroupManager', function ($q, $rootScope, $location, $filter, $timeout, $sce, ApiUpdatesManager) {
  	function getGroupMembers($scope) {
      var parent = $scope;

      while (!parent.chatFull && parent.parent && !parent.participants) {
        parent = $parent.parent
      }

      var groupMembers = parent.participants

      if (!groupMembers && parent.chatFull) {
        if (parent.chatFull.participants) {
          groupMembers = parent.chatFull.participants.participants
        }
      }

      return groupMembers
    }
  })