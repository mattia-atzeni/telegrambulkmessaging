[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Telegram SocialWolf

The app is based on the AngularJS JavaScript framework, and written in pure JavaScript. jQuery is used for DOM manipulations, and Bootstrap as the CSS-framework.


### Running locally

The project repository is based on AngularJS and includes gulp tasks, so it's easy to launch the app, provided that [node.js](http://nodejs.org/) is locally installed.

Dependencies can be installed running the following command:

```
npm install
```

Optionally, run the following command from the project directory to install gulp globally:

```l
sudo npm install -g gulp
```

To start the web server, just run `npm start` (`gulp watch`).
Then, the application should be available at [http://localhost:8000/app/index.html](http://localhost:8000/app/index.html).



#### Running as Chrome Packaged App

To run this application in Google Chrome as a packaged app, open this URL (in Chrome): `chrome://extensions/`, then tick "Developer mode" and press "Load unpacked extension...". Select the downloaded `app` folder and Telegram SocialWolf should appear in the list.

Run `npm start` (`gulp watch`) to watch for file changes and automatically rebuild the app.


#### Running as Firefox OS App

To run this application in Firefox as a packaged app, open "Menu" -> "Developer" -> "WebIDE" (or hit `Shift + F8`). Choose "Open packaged app" from the Project menu and select the `app` folder.

Run `npm start` (`gulp watch`) to watch for file changes and automatically rebuild the app.

#### Running in production

Run `npm run clean` (`gulp clean`), then `npm run publish` (`gulp publish`) to build the minimized production version of the app. Copy `dist` folder contents to your web server. Don't forget to set `X-Frame-Options SAMEORIGIN` header ([docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/X-Frame-Options)).
